//
//  ViewController.swift
//  Tapper
//
//  Created by Zongyue Chen on 11/8/15.
//  Copyright © 2015 Zongyue Chen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var maxTap:Int = 0
    var currentTap:Int = 0
    let alterTitle = "Error!"
    let errorMessage = "Please enter a number bigger than 0"
    @IBAction func playBtnPressed(sender: UIButton!){
        guard let userInputText = tapInput.text where userInputText != "",
        let userInputNum = Int(userInputText) where userInputNum > 0 else {
            
            inputAlert(errorMessage)
            return
        }
        
        
        maxTap = userInputNum

        taperLogo.hidden = true
        playBtn.hidden = true
        tapInput.hidden = true
        tapBtn.hidden = false
        tapCounter.hidden = false

    }
    
    @IBAction func tapBtnPressed(sender: UIButton!){
        currentTap++
        if currentTap < maxTap {
            tapCounter.text = "\(currentTap) Taps"
        }else{
            resetGame()
        }
    }
    
    func inputAlert(title : String){
        //Create alert controller and put the result string in!
        let myAlert = UIAlertController(title: alterTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        //Add an "OK" button to dismiss the alert popup
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myAlert.addAction(alertAction)
        
        //Finally, Show the alert
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
    func resetGame(){
        maxTap = 0
        currentTap = 0
        tapCounter.text = "0 Taps"
        tapInput.text = ""
        taperLogo.hidden = false
        playBtn.hidden = false
        tapInput.hidden = false
        tapBtn.hidden = true
        tapCounter.hidden = true
    }
    

    @IBOutlet weak var taperLogo: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var tapInput: UITextField!
    @IBOutlet weak var tapBtn: UIButton!
    @IBOutlet weak var tapCounter: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

